use libc::{ c_char, c_int, c_void };
use std::ffi::{CStr};
use std::ffi::CString;


type PHYSFS_uint32 = ::libc::c_uint;
type PHYSFS_sint32 = ::libc::c_int;

#[cfg(target_pointer_width = "64")]
type PHYSFS_uint64 = ::libc::c_ulonglong;
#[cfg(target_pointer_width = "64")]
type PHYSFS_sint64 = ::libc::c_longlong;

// PhysFS defines the 64-bit types to 32 bits on 32-bit systems.
#[cfg(target_pointer_width = "32")]
type PHYSFS_uint64 = ::libc::c_uint;
#[cfg(target_pointer_width = "32")]
type PHYSFS_sint64 = ::libc::c_int;


#[repr(C)] 
struct PHYSFS_File { _private: [u8; 0] }


#[repr(C)]
#[allow(non_snake_case)]
struct PHYSFS_ArchiveInfo
{
    extension: *const c_char,
    description: *const c_char,
    author: *const c_char,
    url: *const c_char,
    supportsSymlinks: c_int
}

#[repr(C)]
#[allow(non_camel_case_types)]
enum PHYSFS_FileType
{
    REGULAR,
    DIRECTORY,
    SYMLINK,
    OTHER
}

#[repr(C)]
struct PHYSFS_Stat
{
    filesize: i64,
    modtime: i64,
    createtime: i64,
    accesstime: i64,
    filetype: FileType,
    readonly: c_int
}

#[repr(C)]
#[allow(non_camel_case_types)]
pub enum PHYSFS_ErrorCode
{
    OK = 0,
    OTHER_ERROR,
    OUT_OF_MEMORY,
    NOT_INITIALIZED,
    IS_INITIALIZED,
    ARGV0_IS_NULL,
    UNSUPPORTED,
    PAST_EOF,
    FILES_STILL_OPEN,
    INVALID_ARGUMENT,
    NOT_MOUNTED,
    NOT_FOUND,
    SYMLINK_FORBIDDEN,
    NO_WRITE_DIR,
    OPEN_FOR_READING,
    OPEN_FOR_WRITING,
    NOT_A_FILE,
    READ_ONLY,
    CORRUPT,
    SYMLINK_LOOP,
    IO,
    PERMISSION,
    NO_SPACE,
    BAD_FILENAME,
    BUSY,
    DIR_NOT_EMPTY,
    OS_ERROR,
    DUPLICATE,
    BAD_PASSWORD,
    APP_CALLBACK
}

extern {
    fn PHYSFS_init( argv0: *const c_char ) -> c_int;
    fn PHYSFS_deinit( ) -> c_int;
    fn PHYSFS_supportedArchiveTypes() -> *const *const PhysFSArchiveInfo;
    fn PHYSFS_freeList( listVar: *mut c_void );
    fn PHYSFS_getDirSeparator( ) -> *const c_char;
    fn PHYSFS_getLastErrorCode( ) -> PHYSFSErrorCode;
    fn PHYSFS_getErrorByCode( code: PHYSFSErrorCode ) -> *const c_char;
    fn PHYSFS_permitSymbolicLinks( allow: c_int );
    fn PHYSFS_getCdRomDirs( ) -> *const *const c_char;
    fn PHYSFS_getBaseDir( ) -> *const c_char;
    fn PHYSFS_getWriteDir( ) -> *const c_char;
    fn PHYSFS_setWriteDir( newDir: *const c_char ) -> c_int;
    fn PHYSFS_getSearchPath( ) -> *const *const c_char;
    fn PHYSFS_setSaneConfig( organization: *const c_char, appName: *const c_char, archiveExt: *const c_char, includeCdRoms: c_int, archivesFirst: c_int ) -> c_int;
    fn PHYSFS_mkdir( dirName: *const c_char ) -> c_int;
    fn PHYSFS_delete( filename: *const c_char ) -> c_int;
    fn PHYSFS_getRealDir( filename: *const c_char ) -> *const c_char;
    fn PHYSFS_enumerateFiles( dir: *const c_char ) -> *const *const c_char;
    fn PHYSFS_exists( fname: *const c_char ) -> c_int;
    fn PHYSFS_openWrite( filename: *const c_char ) -> *mut File;
    fn PHYSFS_openAppend( filename: *const c_char ) -> *mut File;
    fn PHYSFS_openRead( filename: *const c_char ) -> *mut File;
    fn PHYSFS_close( handle: *mut File ) -> c_int;
    fn PHYSFS_eof( handle: *mut File ) -> c_int;
    fn PHYSFS_tell( handle: *mut File ) -> i64;
    fn PHYSFS_seek( handle: *mut File, pos: u64 ) -> c_int;
    fn FileLength( handle: *mut File ) -> i64;
    fn PHYSFS_setBuffer( handle: *mut File, bufsize: u64 ) -> c_int;
    fn PHYSFS_flush( handle: *mut File ) -> c_int;
    fn PHYSFS_symbolicLinksPermitted( ) -> c_int;
    fn PHYSFS_mount( newDir: *const c_char, mountPoint: *const c_char, appendToPath: c_int ) -> c_int;
    fn PHYSFS_getMountPoint( dir: *const c_char ) -> *const c_char;
    fn PHYSFS_unmount( oldDir: *const c_char ) -> c_int;
    fn PHYSFS_stat( fname: *const c_char, stat: *mut Stat ) -> c_int;
    fn PHYSFS_readBytes( handle: *mut File, buffer: *mut c_void , len: u64 ) -> i64;
    fn PHYSFS_writeBytes( handle: *mut File, buffer: *const c_void, len: u64 );
    fn PHYSFS_setErrorCode( code: ErrorCode );
    fn PHYSFS_getPrefDir( org: *const c_char, app: *const c_char ) -> *const c_char;
    fn PHYSFS_isInit( ) -> c_int;
}

pub impl PHYSFS_ErrorCode{
    pub fn to_str( code: PHYSFS_ErrorCode ) -> &'static str {
        unsafe{
            let c_str = PHYSFS_getErrorByCode( code ); 
            CStr::from_ptr(c_str).to_str().unwrap() 
        }
    }
}

pub type PhysFSResult = Result<(), PhysFSErrorCode>;

fn last_error_code() -> ErrorCode {
    unsafe{
        PHYSFS_getLastErrorCode()
    }
}

fn check_result( result: c_int ) -> PhysFSResult{
    if result != 0 {
        Ok(())
    } else {
        Err( last_error_code() )
    }
}

fn to_vec<T>( array: *const *const T ) -> Vec<T> {
    unsafe{
        for i in 0 .. {
            let dir: *const c_char = *(array.offset(i));
            if dir != std::ptr::null() {
                let member: &CStr = CStr::from_ptr(dir) ;
                state.push( member.to_str().unwrap() );
                state.set_fieldi( -1, i as i32 );
            }else{
                break;
            }
        }
    }
}

pub fn init( argv0: &str ) -> PhysFSResult {
    let c_str = CString::new(argv0).unwrap();
    let result = unsafe{ PHYSFS_init( c_str.into_raw() ) };
    check_result( result )
}

pub fn deinit() -> PhysFSResult {
    let result = unsafe{ PHYSFS_deinit( ) };
    check_result( result )
}

pub fn is_init( ) -> bool {
    unsafe{ PHYSFS_isInit(  ) } != 0
}

pub fn init_safe( argv0: &str ) -> PhysFSResult {
    if is_init() {
        Ok(())
    } else {
        init( argv0 )
    }
}

pub fn deinit_safe( ) -> PhysFSResult{
    if is_init() {
        deinit()
    } else {
        Ok(())
    }
}

pub fn get_dir_seperator() -> &'static str {
    unsafe{
        let c_str = PHYSFS_getDirSeparator(); 
        CStr::from_ptr(c_str).to_str().unwrap() 
    }
}

pub fn permit_symbolic_links( allow: bool ) {
    unsafe{
        let all = if allow { 1 } else { 0 };
        PHYSFS_permitSymbolicLinks( all );
    }
}

