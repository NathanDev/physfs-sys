use libc::{ c_char, c_int, c_void };
use std::ffi::{CStr};
use std::ffi::CString;


#[repr(C)] 
pub struct File { _private: [u8; 0] }


#[repr(C)]
#[allow(non_snake_case)]
pub struct ArchiveInfo
{
    extension: *const c_char,
    description: *const c_char,
    author: *const c_char,
    url: *const c_char,
    supportsSymlinks: c_int
}

#[repr(C)]
#[allow(non_camel_case_types)]
pub enum FileType
{
    REGULAR,
    DIRECTORY,
    SYMLINK,
    OTHER
}

#[repr(C)]
pub struct Stat
{
    filesize: i64,
    modtime: i64,
    createtime: i64,
    accesstime: i64,
    filetype: FileType,
    readonly: c_int
}

#[repr(C)]
#[allow(non_camel_case_types)]
pub enum ErrorCode
{
    OK = 0,
    OTHER_ERROR,
    OUT_OF_MEMORY,
    NOT_INITIALIZED,
    IS_INITIALIZED,
    ARGV0_IS_NULL,
    UNSUPPORTED,
    PAST_EOF,
    FILES_STILL_OPEN,
    INVALID_ARGUMENT,
    NOT_MOUNTED,
    NOT_FOUND,
    SYMLINK_FORBIDDEN,
    NO_WRITE_DIR,
    OPEN_FOR_READING,
    OPEN_FOR_WRITING,
    NOT_A_FILE,
    READ_ONLY,
    CORRUPT,
    SYMLINK_LOOP,
    IO,
    PERMISSION,
    NO_SPACE,
    BAD_FILENAME,
    BUSY,
    DIR_NOT_EMPTY,
    OS_ERROR,
    DUPLICATE,
    BAD_PASSWORD,
    APP_CALLBACK
}

extern {
    fn PHYSFS_init( argv0: *const c_char ) -> c_int;
    fn PHYSFS_deinit( ) -> c_int;
    fn PHYSFS_isInit( ) -> c_int;
    fn PHYSFS_freeList( listVar: *mut c_void );
    fn PHYSFS_getDirSeparator( ) -> *const c_char;
    fn PHYSFS_getLastErrorCode( ) -> ErrorCode;
    fn PHYSFS_getErrorByCode( code: ErrorCode ) -> *const c_char;
    fn PHYSFS_permitSymbolicLinks( allow: c_int );
    fn PHYSFS_getCdRomDirs( ) -> *const *const c_char;
    fn PHYSFS_getBaseDir( ) -> *const c_char;
    fn PHYSFS_getWriteDir( ) -> *const c_char;
    fn PHYSFS_setWriteDir( newDir: *const c_char ) -> c_int;
    fn PHYSFS_getSearchPath( ) -> *const *const c_char;
    fn PHYSFS_setSaneConfig( organization: *const c_char, appName: *const c_char, archiveExt: *const c_char, includeCdRoms: c_int, archivesFirst: c_int ) -> c_int;
    fn PHYSFS_mkdir( dirName: *const c_char ) -> c_int;
    fn PHYSFS_delete( filename: *const c_char ) -> c_int;
    fn PHYSFS_getRealDir( filename: *const c_char ) -> *const c_char;
    fn PHYSFS_enumerateFiles( dir: *const c_char ) -> *const *const c_char;
    fn PHYSFS_exists( fname: *const c_char ) -> c_int;
    fn PHYSFS_openWrite( filename: *const c_char ) -> *mut File;
    fn PHYSFS_openAppend( filename: *const c_char ) -> *mut File;
    fn PHYSFS_openRead( filename: *const c_char ) -> *mut File;
    fn PHYSFS_close( handle: *mut File ) -> c_int;
    fn PHYSFS_eof( handle: *mut File ) -> c_int;
    fn PHYSFS_tell( handle: *mut File ) -> i64;
    fn PHYSFS_seek( handle: *mut File, pos: u64 ) -> c_int;
    fn FileLength( handle: *mut File ) -> i64;
    fn PHYSFS_setBuffer( handle: *mut File, bufsize: u64 ) -> c_int;
    fn PHYSFS_flush( handle: *mut File ) -> c_int;
    fn PHYSFS_symbolicLinksPermitted( ) -> c_int;
    fn PHYSFS_mount( newDir: *const c_char, mountPoint: *const c_char, appendToPath: c_int ) -> c_int;
    fn PHYSFS_getMountPoint( dir: *const c_char ) -> *const c_char;
    fn PHYSFS_unmount( oldDir: *const c_char ) -> c_int;
    fn PHYSFS_stat( fname: *const c_char, stat: *mut Stat ) -> c_int;
    fn PHYSFS_readBytes( handle: *mut File, buffer: *mut c_void , len: u64 ) -> i64;
    fn PHYSFS_writeBytes( handle: *mut File, buffer: *const c_void, len: u64 );
    fn PHYSFS_setErrorCode( code: ErrorCode );
    fn PHYSFS_getPrefDir( org: *const c_char, app: *const c_char ) -> *const c_char;
}

impl From<c_int> for ErrorCode{
    fn from( v: c_int ) -> ErrorCode{
        match v{
            0 => ErrorCode::OK,
            1 => ErrorCode::OTHER_ERROR,
            2 => ErrorCode::OUT_OF_MEMORY,
            4 => ErrorCode::NOT_INITIALIZED,
            5 => ErrorCode::IS_INITIALIZED,
            6 => ErrorCode::ARGV0_IS_NULL,
            7 => ErrorCode::UNSUPPORTED,
            8 => ErrorCode::PAST_EOF,
            9 => ErrorCode::FILES_STILL_OPEN,
            10 => ErrorCode::INVALID_ARGUMENT,
            11 => ErrorCode::NOT_MOUNTED,
            12 => ErrorCode::NOT_FOUND,
            13 => ErrorCode::SYMLINK_FORBIDDEN,
            14 => ErrorCode::NO_WRITE_DIR,
            15 => ErrorCode::OPEN_FOR_READING,
            16 => ErrorCode::OPEN_FOR_WRITING,
            17 => ErrorCode::NOT_A_FILE,
            18 => ErrorCode::READ_ONLY,
            19 => ErrorCode::CORRUPT,
            20 => ErrorCode::SYMLINK_LOOP,
            21 => ErrorCode::IO,
            22 => ErrorCode::PERMISSION,
            23 => ErrorCode::NO_SPACE,
            24 => ErrorCode::BAD_FILENAME,
            25 => ErrorCode::BUSY,
            26 => ErrorCode::DIR_NOT_EMPTY,
            27 => ErrorCode::OS_ERROR,
            28 => ErrorCode::DUPLICATE,
            29 => ErrorCode::BAD_PASSWORD,
            30 => ErrorCode::APP_CALLBACK,
            _ => ErrorCode::OTHER_ERROR
        }
    }
}

impl ErrorCode{
    pub fn to_str( code: ErrorCode ) -> &'static str {
        unsafe{
            let c_str = PHYSFS_getErrorByCode( code ); 
            CStr::from_ptr(c_str).to_str().unwrap() 
        }
    }
}

type PhysfsResult = Result<(), ErrorCode>;

fn last_error_code() -> ErrorCode {
    unsafe{
        PHYSFS_getLastErrorCode()
    }
}

fn check_result( result: c_int ) -> PhysfsResult{
    if result != 0 {
        Ok(())
    } else {
        Err( last_error_code() )
    }
}

pub fn init( argv0: &str ) -> PhysfsResult {
    let c_str = CString::new(argv0).unwrap();
    let result = unsafe{ PHYSFS_init( c_str.into_raw() ) };
    check_result( result )
}

pub fn deinit() -> PhysfsResult {
    let result = unsafe{ PHYSFS_deinit( ) };
    check_result( result )
}

pub fn is_init( ) -> bool {
    if unsafe{ PHYSFS_isInit(  ) } == 0 {
        false
    } else {
        true
    }
}

pub fn get_dir_seperator() -> &'static str {
    unsafe{
        let c_str = PHYSFS_getDirSeparator(); 
        CStr::from_ptr(c_str).to_str().unwrap() 
    }
}

pub fn permit_symbolic_links( allow: bool ) {
    unsafe{
        let all = if allow { 1 } else { 0 };
        PHYSFS_permitSymbolicLinks( all );
    }
}

